
from prettytable import PrettyTable

menuTable = PrettyTable()
promoTable = PrettyTable()
receiptTable = PrettyTable()


menuTable.field_names = ["item", "price"]
promoTable.field_names = ["item", "price"]
receiptTable.field_names = ["categories", "item", "qty", "price", "total"]

# Initialize variables
selectTable= 'all'
total = 0
order = ""
selectedCategories = ""
selectedCat = 10
# Dictionary containing items and their prices
canMore = True
categories = [
  {
    "name": "all_items",
    "printName": "All Items",
    "priceList": [
      {
        "item": "susu",
        "price": 50000
      },
      {
        "item": "daging",
        "price": 20000
      },
      {
        "item": "lampu",
        "price": 15000
      },
      {
        "item": "masker",
        "price": 25000
      },
      {
        "item": "apel",
        "price": 30000
      }
    ]
  },
  {
    "name": "promotional_items",
    "printName": "Promotional Items",
    "priceList": [
      {
        "item": "susu",
        "price": 50000
      },
      {
        "item": "masker",
        "price": 25000
      }
    ]
  }
]

def printMenu (chooseTable):
    if chooseTable == "all_items":
        menuTable.clear_rows()
        print("Categories & Menu:\n")
        # print(menu2[i])
        print(f"[1] {categories[0]['printName']}")
        for i in categories[0]['priceList']:
            # print(i["item"])
            menuTable.add_row([i["item"], i["price"]])
        print(menuTable)
        print('\n')
        return
    elif chooseTable == "promotional_items":
        promoTable.clear_rows()
        print(f"[2] {categories[1]['printName']}")
        for i in categories[1]['priceList']:
            promoTable.add_row([i["item"], i["price"]])
        print(promoTable)
        print('\n')
        return

    elif chooseTable == "all":
        menuTable.clear_rows()
        print("Categories & Menu:\n")
        # print(menu2[i])
        print(f"[1] {categories[0]['printName']}")
        for i in categories[0]['priceList']:
            # print(i["item"])
            menuTable.add_row([i["item"], i["price"]])
        print(menuTable)
        print('\n')
        promoTable.clear_rows()
        print(f"[2] {categories[1]['printName']}")
        for i in categories[1]['priceList']:
            promoTable.add_row([i["item"], i["price"]])
        print(promoTable)
        print('\n')
        
        return
   
    


def addToCheckout (category, name, price):
    global total, order
    order_qty = int(input("How many would you like? "))
    # Calculate subtotal
    order_price = price * order_qty
    total += order_price
    # Add order to receipt
    receiptTable.add_row([category, name, order_qty, price, order_price])
    order += f"Categories: {category} {order_qty } {name} - Rp{order_price:.2f}\n"
    print(order)
    return


def takeOrder(data):
    # Take order
    global canMore
    order_item = input(f"What menu in {data['printName']} do you want? ")
    if order_item not in [d['item'] for d in data['priceList']]:
        canMore = False
        print(f"{order_item} is not in the data {data['printName']}.")
    for i, d in enumerate(data['priceList']):
        if d['item'] == order_item:
            canMore = True
            addToCheckout({data['printName']}, order_item, data['priceList'][i]['price'] )
            break

# Loop to take orders
while True:
    printMenu(selectTable)
    # Select Category
    if selectedCat == 10:
        option = input("Select your Category (1-2): ")
        if option == "1":
            selectTable = categories[0]['name']
            printMenu(categories[0]['name'])
            print(f"You selected  {categories[0]['printName']}.")
            selectedCategories = categories[0]
            selectedCat = 0
        elif option == "2":
            selectTable = categories[1]['name']
            printMenu(categories[1]['name'])
            print(f"You selected  {categories[1]['printName']}.")
            selectedCategories = categories[1]
            selectedCat = 1
        else:
            print("Invalid choice.")

    if selectedCategories   :
        canMore = True
        takeOrder(selectedCategories)
    else:
        canMore = False

    if canMore == True:
        more_orders = input("Would you like to order more or back to choose category  (y/n/back) ")
        if more_orders.lower() == "n":
            # print(f"\nReceipt:\n{order}Total: Rp{total:.2f}")
            print([receiptTable])
            print(f"Total = Rp{total}")
            break  
        elif more_orders == "back":
            selectedCat =10
            selectTable ='all'





# Print receipt
